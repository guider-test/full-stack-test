## Your decisions logs

### Create a dayCard component
- Q: Should create styles now?
- A: Styles seem simple: yes

- Q: Should implement logic at this stage?
- A: Start implementing loging within component. Move it and improve it later when tackling utils functions.

- Q: Should implement datetime shemas? 
- A: Not enough time

- Q: Should style using rem, px or %? 
- A: Using pixel for the time being.

### Create a timeLabel component
- Let's move some of the styles to the calendar component later.

### Create a calendar component
- Choice: there are lots of libraries to handle date and time in javascript, being moment probably the most famous. I choose not to use any library in the front-end: it's generally better not to load a whole library if not necessary and this task is relatively simple.

- Q: shall I start to interact with the API now? (or create a barebone component first?)

- Observation: Dealing with dates formatting. Probably easiest way to proceed is to convert to string matching the format of `generateMockUpResponse()`.

- Observation: request blocked by cors policy. Just to quickly and temporary solve the problem I have installed and used the cors library. Obviosuly not a final solution.

- Choice: to save time I am not going to build a full time row for the moment, just show the available slots.

- Choice: to save time I am simply using a `float: left` layout 

## Create a library handle time conversion to use with dayCard and timeLabel
- Observation: partially already done, mainly move things in their own directories

---

## Schedule

### Router
- [X] Install react-router-dom and setup the router

### Components
- [X] Create a dayCard component
  - Implement 3 states of a single card: unavailable, available, selected
  - Should use external methods to convert date to the correct format: currently implemented within component, will move later to it's own function.

- [X] Create a timeLabel component
  - Should get prop time from parent component
  - Should get prop status from parent component

- [X] Create a calendar component
  - Show times row on click
  - Show dates row on click
  - Put dates and times in a row 

### Utils
- [X] Create a library handle time conversion to use with dayCard and timeLabel
- [ ] Create a simple library to handle API requests **Won't do**

### Backend
- [X] Grab startDate and endDate from URL and convert them to utc luxon DateTime objects
- [ ] Create function to convert data returned from `generateOffice365Schedule` to requested date format

---

## Notes
- Check the luxon library
- generateOffice365Schedule is declared in 