import React from 'react';
import { explodeDate } from '../utils/dates';
import '../styles/components/DayCard.css';

const DayCard = (props) => {

    /**
     * @description Passes the handleClick to the parent component handler.
     * @param {object} e  
     */
    const handleClick = (e) => {
        if (typeof props.onClick === "function") {
            props.onClick(e);
        }
    }
    
    const { date, weekDay, suffix } = explodeDate(props.date);

    const className = "calendar__day-card" + (props.status ? ` ${props.status}` : "");

    return (date && weekDay) ? (
        <div className={className} onClick={handleClick}>
            <div className="calendar__day-card__date">
                {date}<span>{suffix}</span>
            </div>
            <div className="calendar__day-card__weekday">
                {weekDay}
            </div>
        </div>
    ) : "";
}

export default DayCard;