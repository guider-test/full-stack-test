import React, { Component } from 'react';
import DayCard from './DayCard';
import TimeLabel from './TimeLabel';
import '../styles/components/Calendar.css';

class Calendar extends Component {
    mappedAvailability = {}

    constructor(props) {
        super(props);

        this.state = {
            weekDays: undefined,
            availability: undefined,
            availableTimeSlots: [],
            selectedDay: undefined
        }
    }

    /**
     * @description Performs a request to the APIs to retrieve availabilities.
     */
    getAvailabilities = () => {
        const baseUrl = "http://localhost:8080/";
        const endPoint = baseUrl + "availability";

        return fetch(endPoint).then((availability) => {
            return availability.json();
        }).then((availability) => {
            return availability;
        }).catch((err) => {
            // Some error handling here...
            console.log('Something went wrong!');
        });
    }

    /**
     * @description On DayCard click set the selected day and available time slots.
     * @param {string} selectedDay
     */
    handleDayCardClick = (selectedDay) => {
        console.log(this.mappedAvailability[selectedDay]);
        this.setState({ 
            selectedDay,
            availableTimeSlots: this.mappedAvailability[selectedDay]
         });
    }

    /**
     * @description Builds an array of days of the week.
     * @returns {Array}
     */
    getDaysOfTheWeek = () => {
        let curr = new Date 
        let weekDays = []

        for (let i = 1; i <= 7; i++) {
            let first = curr.getDate() - curr.getDay() + i;
            let day = new Date(curr.setDate(first)).toLocaleString("en-GB").slice(0, 10)
            weekDays.push(day)
        }

        return weekDays;
    }

    /**
     * @description Builds an object having the day string as a key and time slots as content.
     *              This is to easily match days and available slots.
     * @returns {object}
     */
    filterAvailabilities = (availability) => {
        availability.map((day) => {
            this.mappedAvailability[day.date] = day.availableSlots;
        });
    }

    /**
     * @description Given a DayCard day value determines if the status is "selected"
     *              "available" or "unavailable";
     * @returns {string}
     */
    getStatus = (day) => {
        if (day === this.state.selectedDay) {
            return "selected";
        }

        if (this.mappedAvailability[day]) {
            return "available";
        }

        return "unavailable";
    }

    async componentDidMount() {
        const weekDays = this.getDaysOfTheWeek();
        const availability = await this.getAvailabilities();

        this.filterAvailabilities(availability);
        this.setState({ weekDays, availability });
    }

    render() {
        console.log("###00", this.state.availableTimeSlots);
        return (
            <div className="calendar">
                <div className="calendar__slots-row">
                    {this.state.weekDays &&  this.state.weekDays.map((day) => (
                        <DayCard 
                            date={day} 
                            key={day} 
                            onClick={() => this.handleDayCardClick(day)}
                            status={this.getStatus(day)}
                        />
                    ))}
                </div>
                <div className="calendar__slots-row">
                    {this.state.availableTimeSlots.length ? this.state.availableTimeSlots.map(timeSlot => (
                        <TimeLabel time={timeSlot.startTime} />
                    )) : ""}
                </div>
            </div>
        )
    }
}

export default Calendar;