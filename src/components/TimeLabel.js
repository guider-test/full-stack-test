import React from 'react';
import '../styles/components/TimeLabel.css';

const TimeLabel = (props) => {
    const handleClick = (e) => {
        if (typeof props.onClick === "function") {
            props.onClick(e);
        }
    }

    const className = "calendar__time-label" + (props.status ? ` ${props.status}` : "");

    return (
        <div className={className} onClick={handleClick}>{props.time}</div>
    );
}

export default TimeLabel;