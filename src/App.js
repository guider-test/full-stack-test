import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Calendar from './components/Calendar';
import './App.css';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/calendar" component={Calendar} />
      </Switch>
    </div>
  );
}

export default App;
