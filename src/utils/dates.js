/**
 * @param {object} date A Date object.
 * @returns {string}
 */
const getWeekDay = (date) => {
    return date.toDateString().slice(0, 3);
}
/**
 * Given a string representation of a day of the month it returns appropriate suffix.
 * @param {string} day 
 */
const getDaySuffix = (day) => {
    switch (day) {
        case "01":
            return "th";
        case "02":
            return "nd";
        case "03":
            return "rd";
        default:
            return "th";
    }
}

/**
 * @description Given a string representation of a string returns a day and a day of the week.
 * @param {string} date `dd/mm/yyyy` formatted date  
 */
const explodeDate = (date) => {
    const dateArray = date && date.split("/");
    
    if (!dateArray || !dateArray.length) {
        return {};
    }

    const day = dateArray[0];
    const month = dateArray[1];
    const year = dateArray[2];

    return {
        date: day,
        weekDay: getWeekDay(new Date(year, month, day)),
        suffix: getDaySuffix(day)
    }
}

export { getWeekDay, explodeDate }