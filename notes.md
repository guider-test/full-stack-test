### Remaining tasks
The assignment, as expected, has not been completed. There are a few tasks left and I am more than happy to discuss the implementation with them: 
  - The time slots shown are only those which are available. I was planning on using the same technique I have used for the days of the week.
  - The APIs accepts two query string parameters `?startDate=01/04/2020&endDate=10/04/2020` and returns data from the `generateOffice365Schedule()` function, remaining task is to convert this to the data format requested.
  
### Notes
- I have made several commits, so you can see the timeline of my work
- I have fragmented the task in several gitlab issues, each of them connected to a pull request, please see the issue [board](https://gitlab.com/guider-test/full-stack-test/-/boards)
- I have tried to "think out of loud" updating often the decisions.md file
- Finally, I am really interested in this role, seems a great opportunity to join a start up on a relatively early stage but with already a portfolio of great clients. Also it is an interesting concept to work on, in line with some of my previous B2B experiences (building intranets, web service for musicians, and web service for researchers and funders).
I hope that a decision will not be made without first talking to me once more. 